package simplon.blog.monblog.entity;

import jakarta.validation.constraints.Null;

public class Publish {
    private Integer id;
    private String users;
    private String article;
    @Null
    private Integer likes;
    private Integer sectionId;
    private Section section;
    

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Publish() {
    }

    public Publish(String users, String article, Integer likes, Integer sectionId) {
        this.users = users;
        this.article = article;
        this.likes = likes;
        this.sectionId = sectionId;
    }

    public Publish(Integer id, String users, String article, Integer likes, Integer sectionId) {
        this.id = id;
        this.users = users;
        this.article = article;
        this.likes = likes;
        this.sectionId = sectionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }
    
}
