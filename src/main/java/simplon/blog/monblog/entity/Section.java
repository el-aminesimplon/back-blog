package simplon.blog.monblog.entity;

public class Section {
    private Integer id;
    private String titre;
    
    public Section(Integer id, String titre) {
        this.id = id;
        this.titre = titre;
    }

    public Section(String titre) {
        this.titre = titre;
    }

    public Section() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

}
