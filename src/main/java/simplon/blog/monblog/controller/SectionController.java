package simplon.blog.monblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import simplon.blog.monblog.entity.Section;
import simplon.blog.monblog.repository.SectionRepository;


@RestController
@RequestMapping("/api/section")
public class SectionController {

    @Autowired
    private SectionRepository secRepo;

    @GetMapping
    public List<Section> all(){
        return secRepo.findAll();
    }

    @GetMapping("/{id}")
    public Section one(@PathVariable int id) {
        Section sec = secRepo.findById(id);
        if(sec == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return sec;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Section add(@RequestBody Section sec) {
        secRepo.persist(sec);
        return sec;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id){
         if(!secRepo.delete(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
         } 
         secRepo.delete(id);
    }

     @PutMapping("/{id}")
    public Section replace(@PathVariable int id, @RequestBody Section sec) {
        sec.setId(id); 
        if(!secRepo.updateSection(sec)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
         }
        return sec;
    }
}