package simplon.blog.monblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import simplon.blog.monblog.entity.Publish;
import simplon.blog.monblog.repository.PublishRepository;

@RestController
@RequestMapping("/api/publish")
public class PublishController {

    @Autowired
    private PublishRepository pubRepo;

    @GetMapping
    public List<Publish> all(){
        return pubRepo.findAll();
    }


    @GetMapping("/{id}")
    public Publish one(@PathVariable int id) {
        Publish pub = pubRepo.findById(id);
        if(pub == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return pub;
    }

    // GET /api/publish/section/{id}
    //findBySection
    @GetMapping("/section/{id}")
    public List <Publish> oneSection(@PathVariable int id){
        return pubRepo.findBySection(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Publish add(@RequestBody Publish pub) {
        pub.setLikes(0);
        pubRepo.persist(pub);
        return pub;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id){
         if(!pubRepo.delete(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
         }
         pubRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Publish replace(@PathVariable int id, @RequestBody Publish pub) {
        pub.setId(id); 
        if(!pubRepo.updatePublish(pub)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
         }
        return pub;
    }

    @PutMapping("/{id}/like")
    public ResponseEntity<String> addLike(@PathVariable int id) {
        boolean result = pubRepo.addLike(id);
        if (result) {
            return ResponseEntity.ok("Like ajouté avec succès");
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Article non trouvé");
        }
    }

}

