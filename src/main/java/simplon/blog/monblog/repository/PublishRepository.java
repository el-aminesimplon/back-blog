package simplon.blog.monblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import simplon.blog.monblog.entity.Publish;

@Repository
public class PublishRepository {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private SectionRepository sectionRepository;

    public List<Publish> findAll() {
        List<Publish> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM publish");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(sqlToPublish(result));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public List<Publish> findBySection( int id) {
        List<Publish> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM publish WHERE sectionId = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(sqlToPublish(result));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public Publish findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM publish WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return sqlToPublish(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    private Publish sqlToPublish(ResultSet result) throws SQLException {
        Publish publish = new Publish(
                result.getInt("id"),
                result.getString("users"),
                result.getString("articles"),
                result.getInt("likes"),
                result.getInt("SectionId"));
        publish.setSection(sectionRepository.findById(result.getInt("sectionId")));
        return publish;
    }

    public boolean updatePublish(Publish publish) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE publish SET users=?, articles=?, likes=? sectionId=? WHERE id=?");
            stmt.setString(1, publish.getUsers());
            stmt.setString(2, publish.getArticle());
            stmt.setInt(3, publish.getLikes());
            stmt.setInt(4, publish.getSectionId());
            stmt.setInt(5, publish.getId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

    public boolean persist(Publish publish) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO publish (users, articles, likes, sectionId) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, publish.getUsers());
            stmt.setString(2, publish.getArticle());
            stmt.setInt(3, publish.getLikes());
            stmt.setInt(4, publish.getSectionId());

            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                publish.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

    public boolean delete(int id){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM publish WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean addLike(int sectionId) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE publish SET likes = likes + 1 WHERE id = ?");
            stmt.setInt(1, sectionId);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
