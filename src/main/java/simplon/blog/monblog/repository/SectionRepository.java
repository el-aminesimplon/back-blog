package simplon.blog.monblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import simplon.blog.monblog.entity.Section;

@Repository
public class SectionRepository {
    @Autowired
    private DataSource dataSource;

    public List<Section> findAll() {
        List<Section> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM sections");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(sqlToSection(result));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }

    public Section findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM sections WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return sqlToSection(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    private Section sqlToSection(ResultSet result) throws SQLException {
        return new Section(
                result.getInt("id"),
                result.getString("titre"));
    }

    public boolean updateSection(Section section) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE sections SET titre=? WHERE id=?");
            stmt.setString(1, section.getTitre());
            stmt.setInt(2, section.getId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;

    }

    public boolean persist(Section section) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO sections (titre) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, section.getTitre());

            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                section.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

    public boolean delete(int id){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM sections WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
