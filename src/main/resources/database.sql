-- Active: 1709902741465@@127.0.0.1@3306@BLOG

DROP TABLE IF EXISTS user;

DROP TABLE IF EXISTS publish;

DROP TABLE IF EXISTS sections;


CREATE TABLE sections(
    id INT PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(64) NOT NULL
);

CREATE TABLE publish(
    id INT PRIMARY KEY AUTO_INCREMENT,
    users VARCHAR(64) NOT NULL,
    articles TEXT NOT NULL,
    likes INT,
    sectionId INT,
    Foreign Key (sectionId) REFERENCES sections(id) ON DELETE CASCADE
);

INSERT INTO sections (titre) VALUES
("JAVA"), ("HTML"), ("JAVASCRIPT"), ("PHP");

INSERT INTO publish (users, articles, likes, sectionId) VALUES
("Ismaël", "Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.", 12, 2),
("Matthieu", "On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux sites qui n'en sont encore qu'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d'y rajouter de petits clins d'oeil, voire des phrases embarassantes).", 8, 1),
("Cheikh", "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?", 58, 4),
("Felix", "Le lorem ipsum (également appelé faux-texte, lipsum, ou bolo bolo) est, en imprimerie, une suite de mots sans signification utilisée à titre provisoire pour ...", 2, 3);

CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE, -- Pas obligatoire de mettre UNIQUE, pasque le check sera aussi fait par Spring
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);

INSERT INTO user (email,password,role) VALUES 
('admin@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_ADMIN'),
('test@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER');
