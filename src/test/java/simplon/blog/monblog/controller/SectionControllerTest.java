package simplon.blog.monblog.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Sql("/database.sql")
public class SectionControllerTest {
     @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/section"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['titre']").exists());
    }

    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/section/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['titre']").isString());
    }

    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/section/1000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostSection() throws Exception {
        mvc.perform(
            post("/api/publish")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "titre":"REACT",
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }

    @Test
    void testPostInvalidSection() throws Exception {
        mvc.perform(post("/api/section").contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "titre": "JAVASCRIPT",
                }
            """)
            ).andExpect(status().isBadRequest());
    }



    @Test
    void testDeleteSection() throws Exception {
        mvc.perform(delete("/api/section/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutSection() throws Exception {
        mvc.perform(put("/api/section/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "id": 1,
                "titre": "Updated HTML"
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").value(1))
        .andExpect(jsonPath("$['titre']").value("Updated HTML"));
    }
}
