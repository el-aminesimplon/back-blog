package simplon.blog.monblog.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Sql("/database.sql")
public class PublishControlleurTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/publish"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['users']").exists());
    }

    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/publish/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['users']").isString())
        .andExpect(jsonPath("$['article']").isString())
        .andExpect(jsonPath("$['likes']").exists());
    }

    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/dog/1000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostPublish() throws Exception {
        mvc.perform(
            post("/api/publish")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "users": "Anchoura",
                    "article":"hello every one",
                    "likes":"12"
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }



    @Test
    void testDeletePublish() throws Exception {
        mvc.perform(delete("/api/publish/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testDeleteDog() throws Exception {
        mvc.perform(delete("/api/publish/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutPublish() throws Exception {
        mvc.perform(put("/api/publish/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "id": 1,
                "users": "Updated Chien",
                "article":"La breed",
                "likes":"500"
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").value(1))
        .andExpect(jsonPath("$['users']").value("Updated Chien"))
        .andExpect(jsonPath("$['article']").value("La breed"))
        .andExpect(jsonPath("$['likes']").value("500"));
    }
}

